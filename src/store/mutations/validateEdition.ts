import { CustomState } from '@/store/utils/customState';

export function cancelEdition(state: CustomState) {
  state.editedParam = false;
  state.displayEditor = false;
}