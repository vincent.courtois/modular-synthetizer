import { LinkWrapper } from "@/lib/gui/wrappers/LinkWrapper";
import { NodeWrapper } from "@/lib/gui/wrappers/NodeWrapper";
import { CustomState } from "../utils/customState";

export function deleteLink(state: CustomState, link: LinkWrapper): void {
  if(link.end.node instanceof NodeWrapper) {
    (link.start.node.node as any).disconnect(
      link.end.node.node as any,
      link.start.position,
      link.end.position
    )
  }
  else {
    (link.start.node.node as any).disconnect(
      link.end.node.param as any
    )
  }
  const sIndex: number = link.start.connectedTo.indexOf(link);
  link.start.connectedTo.splice(sIndex, 1);
  const tIndex: number = link.end.connectedFrom.indexOf(link);
  link.end.connectedFrom.splice(tIndex, 1);
  const index: number = state.links.indexOf(link);
  state.links.splice(index, 1);
}