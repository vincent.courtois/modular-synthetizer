import { Coordinates } from '@/store/utils/Coordinates';
import { NodeWrapper } from '@/lib/gui/wrappers/NodeWrapper';
import { PortWrapper } from '@/lib/gui/wrappers/PortWrapper';
import { LinkWrapper } from '@/lib/gui/wrappers/LinkWrapper';
import { ParamWrapper } from '@/lib/gui/wrappers/ParamWrapper';

export class CustomState {
  links: LinkWrapper[] = []
  beginTrace?: PortWrapper;
  editedParam: ParamWrapper|boolean = false;
  editConfig: Coordinates = new Coordinates(0, 0);
  displayEditor: boolean = false;
  scale: number = 1;
  nodes: NodeWrapper[] = [];
  dragStart: Coordinates = new Coordinates(0, 0);
  dragDiff: Coordinates = new Coordinates(0, 0);
}