export class Coordinates {
  left: string = "0px";
  top: string = "0px";
  x: number = 0;
  y: number = 0;
  transform: string = "scale(1, 1)";
  oldScale: number = 1;

  constructor(left: number, top: number, scale: number = 1) {
    this.setScale(scale)
    this.setX(left)
    this.setY(top);
  }

  setScale(scale: number) {
    this.transform = `scale(${scale}, ${scale})`
    this.setX(this.x, scale);
    this.setY(this.y, scale);
    this.oldScale = scale;
  }

  setX(x: number, scale: number = 1) {
    this.x = x / this.oldScale * scale;
    this.left = `${this.x}px`
  }

  setY(y: number, scale: number = 1) {
    this.y = y / this.oldScale * scale;
    this.top = `${this.y}px`
  }
}