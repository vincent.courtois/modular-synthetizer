import { NodeWrapper } from '@/lib/gui/wrappers/NodeWrapper';
import { CustomState } from '@/store/utils/customState';
import { Coordinates } from '@/store/utils/Coordinates';
import { LinkWrapper } from '@/lib/gui/wrappers/LinkWrapper';
import { cancelEdition } from '@/store/mutations/validateEdition'
import { deleteLink } from '@/store/mutations/deleteLink'
import Vue from 'vue'
import Vuex from 'vuex'
import { ParamWrapper } from '@/lib/gui/wrappers/ParamWrapper';
import { MAX_ZOOM_IN, MAX_ZOOM_OUT, ZOOM_RATIO } from '@/lib/gui/utils/constants';
import { dragAndDrop } from "./modules/DragAndDrop"
import { connections } from "./modules/Connections"
import { audionodes } from "./modules/AudioNodes"
import { keyboard } from "./modules/Keyboard"

Vue.use(Vuex)

export default new Vuex.Store({
  state: new CustomState(),
  mutations: {
    deleteNode(state, {index, event}) {
      cancelEdition(state);
      const node = (state.nodes[index] as NodeWrapper);
      node.removeLinks();
      state.nodes.splice(index, 1);
      event.cancelBubble = true;
    },
    addLink(state, link: LinkWrapper) {
      state.links.push(link);
      if(link.end.node instanceof ParamWrapper) {
        (link.start.node.node as any).connect(
          link.end.node.param as AudioParam
        );
      }
      else {
        (link.start.node.node as any).connect(
          link.end.node.node as AudioNode,
          link.start.position,
          link.end.position
        );
      }
    },
    hoverLink(state, link: LinkWrapper): void {
      link.config.onHoverChange();
    },
    deleteLink,
    deleteLinks(state, links: LinkWrapper[]) {
      links.forEach((link: LinkWrapper) => {
        deleteLink(state, link);
      })
    },
    setEditedParam(state, wrapper: ParamWrapper) {
      state.displayEditor = false;
      state.editedParam = wrapper;
      state.displayEditor = true;
    },
    setEditPosition(state, position) {
      state.editConfig = new Coordinates(
        position.x + state.dragDiff.x,
        position.y + state.dragDiff.y,
        state.scale
      );
    },
    cancelEdition,
    triggerZoomScaling(state, e) {
      state.scale += e.deltaY * -ZOOM_RATIO;
      state.scale = Math.min(Math.max(MAX_ZOOM_OUT, Math.abs(state.scale)), MAX_ZOOM_IN);
      state.editConfig.setScale(state.scale);
    },
    dragStart(state: CustomState, $event: any) {
      if($event.evt === undefined) return;
      state.dragStart = new Coordinates(
        $event.evt.screenX,
        $event.evt.screenY
      );
    },
    dragEnd(state: CustomState, $event) {
      const diff = state.dragDiff;
      const start = state.dragStart;
      const evt = $event.evt;
      state.dragDiff = new Coordinates(
        diff.x + evt.screenX - start.x,
        diff.y + evt.screenY - start.y
      )
    }
  },
  actions: {
  },
  modules: {
    dragAndDrop,
    connections,
    audionodes,
    keyboard,
  }
})
