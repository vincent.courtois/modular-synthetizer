import { Module } from "vuex"
import { DragAndDropState, state } from "./state"
import { CustomState } from "@/store/utils/customState"
import { mutations } from "./mutations"

export const dragAndDrop: Module<DragAndDropState, CustomState> = {
  namespaced: true,
  state,
  mutations
}