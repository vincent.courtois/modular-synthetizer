export function searchAndDestroy(list: any[], items: any[]) {
  items.forEach(item => {
    const index = list.indexOf(item);
    if (index >= 0) list.splice(index, 1);
  })
}

export function addToList(list: any[], items: any[]) {
  items.forEach(item => {
    if (list.find(i => i === item) === undefined) {
      list.push(item);
    }
  })
}