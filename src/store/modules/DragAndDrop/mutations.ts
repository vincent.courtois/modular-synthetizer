import { MutationTree } from "vuex"
import { CustomState } from "@/store/utils/customState"
import { DragAndDropState } from "./state"
import { NodeWrapper } from "@/lib/gui/wrappers/NodeWrapper"
import { LinkWrapper } from "@/lib/gui/wrappers/LinkWrapper"
import { searchAndDestroy, addToList } from "./utils"
import { DragMoveEvent } from "@/lib/interfaces/DragMoveEvent"

/**
 * Represents the common payload of both methods concernings dragging and dropping.
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
interface IPayload {
  node: NodeWrapper;
  event: any;
}

export const mutations: MutationTree<DragAndDropState> = {
  /**
   * This function exists to fix most of the performances problems due to the drag
   * and drop system we adopted for the GUI. Instead of just using the default
   * drag and drop system from Konva, we isolate the dragged item in another layer
   * before moving it so that only this layer is refreshed when moving the item.
   * The triggerDragEnd method on her side brings back the element in the default
   * layer by re-adding it to the list of nodes.
   * 
   * @param state the state to modify (mandatory vuex parameter)
   * @param node the node the user is trying to drag and drop
   * @param evt the event triggered by Konva when triggering the mousedown event
   *   on the desired node.
   */
  triggerDragStart(state: DragAndDropState, payload: IPayload) {
    const rootState = this.state as unknown as CustomState
    searchAndDestroy(rootState.nodes, [payload.node])
    payload.node.links.forEach((link: LinkWrapper) =>  {
      searchAndDestroy(rootState.links, [link]);
      addToList(state.draggedLinks, [link])
    })
    state.draggedNode = payload.node;
    payload.event.cancelBubble = true;
  },
  /**
   * This method is called on the temporary third layer where the dragged object has
   * been isolated for performance concerns. It brings back the node in the general
   * list, thus in the nodes layer with the others as it won't be dragged so it won't
   * update the whole layer anew.
   * 
   * @param state the state to modify (mandatory vuex parameter)
   * @param node the node the user is trying to drag and drop
   * @param evt the event triggered by Konva when triggering the dragend event
   *   on the desired node.
   */
  triggerDragEnd(state: DragAndDropState, payload: IPayload) {
    // There is a rare case where dragend and mouseup are triggered are roughly
    // The same time, and one fires an undefined node, this handles this case.
    if (payload.node === undefined) return;

    const rootState = this.state as unknown as CustomState;
    addToList(rootState.nodes, [payload.node]);
    rootState.links = [...rootState.links, ...state.draggedLinks]
    state.draggedNode = undefined;
    state.draggedLinks = []
  },
  triggerDragMove({ draggedNode }, event: DragMoveEvent) {
    if (!draggedNode) return;
    const { x, y } = event.target.attrs;
    draggedNode.config.updateCoordinates(x, y);
  }
}