## The drag'n'drop module

### Context

This module holds the logic for dragging and dropping nodes in the main interface.

At first we made the dragging and dropping in the main layers because we did not use a lot of nodes, but after 15-20 nodes and tens of connections, it started having serious performance issues so we decided to make it another way.

### Behaviours

The dragging is now made like this :

1. you trigger the "mousedown" event on a node by clicking on it and not releasing the button
2. the node is deleted from the nodes list and put in a temporary list aside
3. its links are deleted from the main links list and put aside in another temporary list
4. the two temporary lists are redrawn in separate layers
5. the dragging event is triggered manually on the node so that it emulates the drag event from Konva

The dropping is handled like so :

1. you trigger the "dragend" or "mouseup" events by releasing the mouse button, having moved the node or not
2. the node sees its coordinates updated so that it places correctly
3. the node is deleted from the temporary list and put back in the main list
4. its links are put back in the main links list too
5. the layer is un-rendered as there are no more dragged node, to not spend resources on a useless layer

### Structure

* `state.ts` contains the state and its definition.
* `mutations.ts` contains the mutations and the related documentation for each of them.
* `index.ts` creates and exports the module for the main state to use.