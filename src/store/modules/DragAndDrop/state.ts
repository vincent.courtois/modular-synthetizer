import { NodeWrapper } from "@/lib/gui/wrappers/NodeWrapper"
import { LinkWrapper } from "@/lib/gui/wrappers/LinkWrapper"

export class DragAndDropState {
  draggedNode?: NodeWrapper = undefined;
  draggedLinks: LinkWrapper[] = [];
}

export const state: DragAndDropState = new DragAndDropState();