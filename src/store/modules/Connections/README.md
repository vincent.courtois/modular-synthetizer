# The links creation module

### Context

In the ancient times, links were created with a simpler procedure : you first clicked on a port where you wanted to start the link, then on another port where the link could be finished. It had several drawbacks :

* It did not have an indicator telling you you were correctly creating the link with the first click
* It did not tell you why it failed to create the link when you clicked on two inputs, or two outputs for that matter.
* It erased the starting port from memory when you clicked anywhere else on the screen.

### Behaviours

To create a link now, you can do :

1. Click on a starting port
2. While keeping the button clicked, drag the mouse cursor to the ending port you want to create the link to
3. When the cursor has correctly magnetized to the port, you can release the mouse button
4. The link is created !

### Technical considerations

The link is separated in two : the one that comes to the edge of the ending port, and the little bit one that isfrom the edge of the port to the center of the finishing port. This is made like this so that the little bit at the end can be drawn under the replica of the port handling events so that the mouse won't pass on it and trigger `mouseenter` and `mouseout` events all the time, triggering the magnetism without reason.

### Structure

* `state.ts` contains the state and its definition.
* `mutations.ts` contains the mutations and the related documentation for each of them.
* `index.ts` creates and exports the module for the main state to use.