import { PortWrapper } from "@/lib/gui/wrappers/PortWrapper";
import { Line } from "konva/lib/shapes/Line";

export class ConnectionsState {
  linkStart?: PortWrapper;
  tmpLink: any = {
    stroke: "black",
    strokeWidth: 3,
    points: []
  };
  isDrawingLink: boolean = false;
  magnetPort?: PortWrapper;
  magnetized: boolean = false;
  tmpMagnet: any = {
    stroke: "black",
    strokeWidth: 3,
    points: []
  }
}

export const state: ConnectionsState = new ConnectionsState();