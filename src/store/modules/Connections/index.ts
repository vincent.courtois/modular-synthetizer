import { Module } from "vuex"
import { ConnectionsState, state } from "./state"
import { CustomState } from "@/store/utils/customState"
import { mutations } from "./mutations"

export const connections: Module<ConnectionsState, CustomState> = {
  namespaced: true,
  state,
  mutations
}