import { PORTS_INTERVAL, PORTS_RADIUS, WDT } from "@/lib/gui/utils/constants";
import { translate } from "@/lib/gui/utils/translate";
import { PortWrapper } from "@/lib/gui/wrappers/PortWrapper";
import { MutationTree } from "vuex";
import { ConnectionsState } from "./state";
import { drawMagnetizedLink } from "./utils";

interface MagnetismPayload {
  port: PortWrapper;
  isMagnet: boolean;
}

export const mutations: MutationTree<ConnectionsState> = {
  /**
   * This is called when the mousedown is triggered on the starting port
   * for a new link, recording it in memory until the link creation is
   * cancelled.
   * 
   * @param port the beginning port for the link being created.
   */
  setLinkStart(state: ConnectionsState, port: PortWrapper) {
    state.linkStart = port;
    const {x, y} = port.config.absPosition
    state.isDrawingLink = true;
    state.tmpLink.points = [x, y, x, y];
  },
  /**
   * Re-compute the coordinates for the end of the link shen the mouse is moved
   * in the canva.
   * 
   * @param $event the mouse event giving us the coordinates in the current layer
   */
  updateTmpLink(state: ConnectionsState, $event: any) {
    if (state.isDrawingLink && state.tmpLink !== undefined) {
      if (state.magnetized && state.magnetPort !== undefined) {
        drawMagnetizedLink(state.tmpLink, state.tmpMagnet, state.magnetPort);
      }
      else {
        const {x, y} = translate({
          x: $event.evt.layerX,
          y: $event.evt.layerY
        });
        const p = state.tmpLink.points;
        state.tmpLink.points = [p[0], p[1], x, y];
      }
    }
  },
  cancelDrawnLink(state: ConnectionsState) {
    state.magnetized = false;
    state.isDrawingLink = false;
  },
  /**
   * Validates the link by giving it an end and resetting the module. The link
   * is therefore added to the list of links as usual.
   * 
   * @param port the ending port for the currently drawn link.
   */
  validateLink(state: ConnectionsState, port: PortWrapper) {
    if (state.linkStart === undefined) return;
    if (state.linkStart.input === port.input) return;

    if (state.isDrawingLink) {
      if (port.input) state.linkStart.connect(port);
      else port.connect(state.linkStart);
    }
    port.config.displayMagnet = false;
    state.magnetized = false;
    state.isDrawingLink = false;
  },
  /**
   * Sets the magnetism of the port. A port is magnetized if your mouse is inside
   * of it, and unmagnetizes when you get out of it.
   * 
   * @param payload the port you want to magnetize or unmagnetize, and the boolean
   *   saying if you want to flag it as "magnetized" (you enter it) or "unmagnetized"
   *   (you're getting out of it).
   */
  setMagnetism(state: ConnectionsState, payload: MagnetismPayload) {
    if (state.linkStart === undefined) return;
    if (state.linkStart.input === payload.port.input) return;

    if (state.isDrawingLink) {
      state.magnetized = payload.isMagnet;
      payload.port.config.displayMagnet = payload.isMagnet;
      state.magnetPort = payload.port;
    }
  }
}