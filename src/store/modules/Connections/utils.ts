import { PORTS_INTERVAL, PORTS_RADIUS, WDT } from "@/lib/gui/utils/constants";
import { PortWrapper } from "@/lib/gui/wrappers/PortWrapper";

// Simpler representation of a Konva.Line
interface Connection {
  points: number[],
  stroke: string,
  strokeWidth: number
}

interface Position {
  x: number;
  y: number;
}

/**
 * Reconstructs the two parts of the magnetized link between two ports.
 * 1. the part between the start port and the edge of the end port circle
 * 2. the part between the end port circle and the center of the end port
 *
 * @param link the big part of the link, from the center of the starting port
 *   to the edge of the circle of the ending port.
 * @param linkEnd the smaller part of the circle, from the edge of the ending
 *   port to the center of the ending port.
 * @param end the ending port itself as a PortWrapper
 */
export function drawMagnetizedLink(link: Connection, linkEnd: Connection, end: PortWrapper) {
  link.points = computeLinkPoints(link, end);
  linkEnd.points = computeLinkEndPoints(link, end)
}

/**
 * Returns the distance from a starting point to the edge of the port linked to
 * an ending port. It is used to calculate the length of the first part of the link.
 * 
 * @param start the position of the starting port
 * @param end the position of the ending port
 * @returns the distance between the center of the starting port, and the edge of
 *   the ending point, where the point on the edge is the intersection of the link
 *   between the two centers, and the edge of the circle of the port.
 */
function getDistanceFromPortEdge(start: Position, end: Position) {
  return Math.sqrt((start.x - end.x)**2 + (start.y - end.y)**2) - PORTS_RADIUS;
}

/**
 * Draws the link from the center of the starting port, to the edge of the
 * ending port. This part will be above the ports in the canva in z-index.
 */
function computeLinkPoints(link: Connection, end: PortWrapper) {
  const p = link.points;
  const {x, y} = end.config.absPosition;
  const distance = getDistanceFromPortEdge({x: p[0], y: p[1]}, {x, y})
  // The ratio to apply to both coordinates
  const ratio = distance / (distance + PORTS_RADIUS);
  // These coordinates are the ones of the point on the edge of the
  // port circle where the magnetized link crosses the port.
  const endX = p[0] + ratio * (x - p[0])
  const endY = p[1] + ratio * (y - p[1])
  //link.points = [p[0], p[1], endX, endY];
  return [p[0], p[1], endX, endY];
}

/**
 * compute points for the last part of the link, from the edge of the ending
 * port to its center. This will be drawn below the transparent port handling
 * events.
 */
function computeLinkEndPoints(link: Connection, end: PortWrapper) {
  const p = link.points;
  const {x, y} = end.config.absPosition;
  const distance = getDistanceFromPortEdge({x: p[0], y: p[1]}, {x, y})
  const ratio = PORTS_RADIUS / distance;
  // The offset helps placing the little bit of the link that is supposed to
  // be inside the port in the correct port. They represent the origin of
  // the coordinates for the port.
  const offset = {
    x: end.input ? 0 : WDT,
    y: end.position * PORTS_INTERVAL
  }
  // These coordinates are on the edge of the circle too, but it must be
  // considered in the referential of the port, not absolute coordinates.
  const startX = (x - p[0]) * ratio - offset.x;
  const startY = (y - p[1]) * ratio - offset.y;
  return [-startX, -startY, offset.x, offset.y];
}