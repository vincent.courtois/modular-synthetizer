import { NodesState, state } from "./state";
import { mutations } from "./mutations";
import { Module } from "vuex";
import { CustomState } from "@/store/utils/customState";

export const audionodes: Module<NodesState, CustomState> = {
  namespaced: true,
  state,
  mutations
}