import { NodeWrapper } from "@/lib/gui/wrappers/NodeWrapper";
import { CustomState } from "@/store/utils/customState";
import { MutationTree } from "vuex";
import { NodesState } from "./state";

export const mutations: MutationTree<NodesState> = {
  /**
   * Creates a node given the tool desired to initialize it. The tool manages
   * the factory you use to create it, or the component you're going to use
   * to display it to the user.
   * @param tool the tool object you want to use to create the node.
   */
  async createNode(state: NodesState, tool: any) {
    if (state.context !== undefined) {
      const rootState = this.state as unknown as CustomState
      let node = tool.factory(state.context);
      if (node instanceof Promise) node = await node;
      const { wrapper = NodeWrapper } = tool;
      const obj = new wrapper(node, tool.label);
      rootState.nodes.push(obj);
    }
  },
  setContext(state, context: AudioContext) {
    state.context = context;
  },
}
