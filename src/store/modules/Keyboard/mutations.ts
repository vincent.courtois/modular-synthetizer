import { INote, notes } from "@/lib/audio/utils/keyboard";
import { KeyboardWrapper } from "@/lib/gui/wrappers/KeyboardWrapper";
import { NodeWrapper } from "@/lib/gui/wrappers/NodeWrapper";
import { MutationTree } from "vuex";
import { Keys, KeyboardState } from "./state";

const mapping: Record<string, string> = {};

const digitsRegex = /^[0-9]{1}$/

// This bit creates the mapping between the keycodes and the notes.
// Key codes are a bit different of the displayed key, for example
// for the letter C its KeyC, and for 7 it's Digit7.
notes.forEach((note: INote) => {
  mapping[keyCode(note.key)] = note.name;
  if (note.blackKey !== undefined) {
    mapping[keyCode(note.blackKey)] = `${note.name}.black`;
  }
});

function keyCode(key: string) {
  return key.match(digitsRegex) ? `Digit${key}` : `Key${key}`
}

export const mutations: MutationTree<KeyboardState> = {
  keydown(state: KeyboardState, $event: KeyboardEvent) {
    const k = $event.code as keyof Keys
    if (state.keys[k] !== true) {
      state.keys[k] = true;
      (this.state as any).nodes.forEach((node: NodeWrapper) => {
        if (node instanceof KeyboardWrapper) {mapping[$event.code]
          node.attackNote(mapping[$event.code]);
        }
      })
    }
  },
  keyup(state: KeyboardState, $event: KeyboardEvent) {
    const k = $event.code as keyof Keys
    if (!(mapping[k])) return;
    if (state.keys[k] === true) {
      state.keys[k] = false;
      (this.state as any).nodes.forEach((node: NodeWrapper) => {
        if (node instanceof KeyboardWrapper) {
          node.releaseNote(mapping[$event.code]);
        }
      })
    }
  }
};