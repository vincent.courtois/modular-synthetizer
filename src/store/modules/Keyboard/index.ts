import { Module } from "vuex"
import { KeyboardState, state } from "./state"
import { CustomState } from "@/store/utils/customState"
import { mutations } from "./mutations"

export const keyboard: Module<KeyboardState, CustomState> = {
  namespaced: true,
  state,
  mutations
}