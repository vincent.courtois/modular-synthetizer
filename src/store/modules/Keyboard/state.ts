export type Keys = {[key: string]: boolean}

export class KeyboardState {
  keys: Keys = {};
};

export const state: KeyboardState = new KeyboardState();