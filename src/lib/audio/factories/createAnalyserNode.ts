import { VISUALIZER_PRECISION } from "@/lib/gui/utils/constants";

export function createAnalyserNode(context: AudioContext): AnalyserNode {
  const analyser = context.createAnalyser();
  analyser.fftSize = VISUALIZER_PRECISION;
  return analyser;
}
