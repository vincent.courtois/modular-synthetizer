export function createConvolver(context: AudioContext) {
  return context.createConvolver();
}