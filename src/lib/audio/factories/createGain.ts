export function createGain(context: AudioContext): GainNode {
  return context.createGain();
}