import { INote, notes } from "../utils/keyboard";

/**
 * Creates the whole pipeline needed to have a polyphonic keyboard working :
 * - one final gain node that will be our main output for the keyboard
 * - one gain per key that is linked to the final gain, and will control the
 *   volume for this key so that it triggers when it is pressed or released
 * - one oscillator per key connected to the correspo,nding gain, and giving
 *   the pitch of the key.
 * 
 * @param context the audio context in which create the nodes
 * @returns the final gain node and the keys as one object.
 */
export function createKeyboard(context: AudioContext) {
  const node = context.createGain();
  node.gain.setValueAtTime(0.1, 0);
  const keys: any = {};
  notes.forEach((note: INote) => {
    keys[note.name] = createBasicPipeline(context, note.pitch, node);
    if (note.hasBlack && note.black !== undefined) {
      keys[`${note.name}.black`] = createBasicPipeline(context, note.black, node);
    }
  });
  return {node, keys};
}

/**
 * Creates the audio pipeline for one key of the keyboard. The pipeline consists
 * in one gain node and one oscillator node controling the volume of the key
 * and its pitch.
 * 
 * @param context the audio context in which create the nodes
 * @param pitch the frequency, in hertz, of the oscillator
 * @param globalGain the global node gain to connect the pipeline to
 * @returns the couple oscillator/gain created
 */
function createBasicPipeline(context: AudioContext, pitch: number, globalGain: AudioNode) {
  const oscillator = createOscillator(context, pitch);
  const gain = createGain(context);
  oscillator.connect(gain);
  gain.connect(globalGain);
  return {oscillator, gain, played: false};
}

function createOscillator(context: AudioContext, pitch: number) {
  const oscillator = context.createOscillator();
  oscillator.type = "square";
  oscillator.start();
  oscillator.frequency.setValueAtTime(pitch, 0);
  return oscillator;
}

function createGain(context: AudioContext) {
  const gain = context.createGain();
  gain.gain.setValueAtTime(0, context.currentTime);
  return gain;
}