export function createDelay(context: AudioContext) {
  const delay: DelayNode = context.createDelay();
  delay.delayTime.value = 1;
  return delay
}