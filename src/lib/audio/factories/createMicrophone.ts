export async function createMicrophone(context: AudioContext): Promise<MediaStreamAudioSourceNode> {
  return navigator.mediaDevices.getUserMedia({audio: true, video: false}).then(stream => {
    return context.createMediaStreamSource(stream);
  })
}