export function createOscillator(type: string) {
  return function(context: AudioContext): OscillatorNode {
    const oscillator: OscillatorNode = context.createOscillator();
    oscillator.type = type as OscillatorType;
    oscillator.start();
    return oscillator;
  }
}