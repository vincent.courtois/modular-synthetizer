export function createDestination(context: AudioContext): AudioNode {
  return context.destination;
}