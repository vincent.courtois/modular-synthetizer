export function createChannelSplitter(context: AudioContext) {
  return context.createChannelSplitter(4);
}