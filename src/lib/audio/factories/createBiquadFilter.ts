export function createBiquadFilter(type: string) {
  return function(context: AudioContext) {
    const filter: BiquadFilterNode = context.createBiquadFilter();
    filter.type = type as BiquadFilterType;
    return filter;
  }
}