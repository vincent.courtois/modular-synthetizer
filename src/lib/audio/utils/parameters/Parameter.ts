import { IParameter } from "./IParameter"
import LinearStrategy from "./strategies/LinearStrategy";

export class Parameter implements IParameter {
  name: string;
  component: string;
  strategy: string;

  constructor(name: string, component: string) {
    this.name = name;
    this.component = component;
    this.strategy = "linear"
  }
}