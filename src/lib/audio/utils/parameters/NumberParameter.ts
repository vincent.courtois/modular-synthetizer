import { Parameter } from "./Parameter"
import LinearStrategy from "./strategies/LinearStrategy";

/**
 * This is the representation of a numerical parameter, translated in the UI
 * by a slider from the minimal value to the maximal value.
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export class NumberParameter extends Parameter {
  minValue: number;
  maxValue: number;

  constructor(name: string, minValue: number, maxValue: number, strategy: string = "linear") {
    super(name, "NumberInput");
    this.minValue = minValue;
    this.maxValue = maxValue;
    this.strategy = strategy;
  }
}