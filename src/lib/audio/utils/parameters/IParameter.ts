/**
 * This interface describes a standard parameter.
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export interface IParameter {
  name: string;
  component: string;
  strategy: string;
}