import { SLIDER_WIDTH } from "@/lib/gui/utils/constants";
import BasicStrategy from "./IStrategy";

/**
 * Maps the values of the X axis of the slider directly on the
 * delta between minimum and maximum for the associated parameter.
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default class LinearStrategy extends BasicStrategy {
  computeX(value: number): number {
    return (SLIDER_WIDTH / this.delta) * (value - this.minimum) + 30;
  }
  computeValue(x: number): number {
    return (this.delta / SLIDER_WIDTH) * x + this.minimum;
  }
}