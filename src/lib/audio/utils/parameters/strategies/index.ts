import { ParamWrapper } from "@/lib/gui/wrappers/ParamWrapper";
import ExponantialStrategy from "./ExponantialStrategy";
import IStrategy from "./IStrategy";
import LinearStrategy from "./LinearStrategy";

const strategies: {[key: string]: any} = {
  "linear": LinearStrategy,
  "exponantial": ExponantialStrategy
}

export default function getStrategy(name: string, param: ParamWrapper): IStrategy {
  return new (strategies[name] || LinearStrategy)(param);
}