import { ParamWrapper } from "@/lib/gui/wrappers/ParamWrapper";
import { state } from "@/store/modules/AudioNodes/state"

/**
 * Represents a transformation strategy for values in a slider. It gives
 * an interface to transform a value into a X coordinates, and a X coordinates
 * into a usable value so that all strategies are called the same way.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export interface IStrategy {
  delta: number;
  minimum: number;
  
  setValue(x: number): number;
  setX(value: number): number;

  /**
   * Computes the value of the parameter linked to this slider by using a
   * different function for each strategy. This function is to be reimplemented
   * in every strategy for it to properly work.
   * @param x the number of pixels from the start of the slider to compute the
   *   parameter value from.
   * @return the value to set on the parameter linked to this slider.
   */
  computeValue(x: number): number;

  /**
   * Computes the X coordinate of the center of the selector part of the slider
   * from the given value of the parameter. This function is to be reimplemented
   * in every strategy for it to properly work.
   * @param value The value to compute the X coordinate from.
   * @return The number of pixels from the left side of the slider to put the
   *   circle selector at.
   */
  computeX(value: number): number;
}

export default class BasicStrategy implements IStrategy {
  param: ParamWrapper;
  delta: number;
  minimum: number;

  constructor(param: ParamWrapper) {
    this.param = param;
    this.minimum = param.getConfiguration().minValue;
    this.delta = param.config.getBounds().delta;
  }
  computeValue(x: number): number {
    throw new Error("Method not implemented.");
  }
  setX(value: number): number {
    const x = this.computeX(value)
    if (state.context !== undefined) {
      this.param.config.circleConfig.x(x);
    }
    return x;
  }
  computeX(value: number): number {
    throw new Error("Method not implemented.");
  }
  setValue(x: number): number {
    const value = this.computeValue(x)
    this.param.value = value;
    if (state.context !== undefined) {
      this.param.param.setValueAtTime(value, state.context.currentTime);
    }
    return value;
  }
}