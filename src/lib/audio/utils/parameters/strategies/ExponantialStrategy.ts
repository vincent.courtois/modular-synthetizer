import { SLIDER_WIDTH } from "@/lib/gui/utils/constants";
import { ParamWrapper } from "@/lib/gui/wrappers/ParamWrapper";
import BasicStrategy from "./IStrategy";

/**
 * This strategy has more precision in the small values (minimum limit) and
 * less precision in the big values (maximum limit).
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export default class ExponantialStrategy extends BasicStrategy {

  /**
   * The ratio allows the mapping of 0-120 (the width of the slider)
   * on delta (maximum + 1 - minimum; the range values of the slider)
   */
  ratio: number = 0;

  constructor(param: ParamWrapper) {
    super(param);
    this.delta = this.delta + 1;
    this.ratio = Math.log10(this.delta) / SLIDER_WIDTH;
  }
  computeX(value: number): number {
    return Math.log10(value + 1) / this.ratio + 30
  }
  computeValue(x: number): number {
    return 10 ** (x * this.ratio) - 1
  }
}