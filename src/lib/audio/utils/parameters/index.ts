import { NumberParameter } from "./NumberParameter"
import { IParameter } from "./IParameter"
import ExponantialStrategy from "./strategies/ExponantialStrategy"

export enum ParametersEnum {
  OSCILLATOR_NODE = "OscillatorNode",
  GAIN_NODE = "GainNode",
  DELAY_NODE = "DelayNode",
  BIQUAD_FILTER_NODE = "BiquadFilterNode",
}

const nodeNameToEnum: Record<string, ParametersEnum> = {
  Oscillator: ParametersEnum.OSCILLATOR_NODE,
  "Square Oscillator": ParametersEnum.OSCILLATOR_NODE,
}

/**
 * This represents the whole parameters for our kind of nodes.
 */
export type ParametersConfiguration = {
  "OscillatorNode": IParameter[],
  "GainNode": IParameter[],
  "DelayNode": IParameter[],
  "BiquadFilterNode": IParameter[]
}


/**
 * The configuration of the application itself, only parameters listed
 * here are correctly displayed as editable and pluggable in the interface.
 */
const parameters: Record<ParametersEnum, IParameter[]> = {
  OscillatorNode: [
    new NumberParameter("frequency", 0, 1000, "exponantial"),
    new NumberParameter("detune", 0, 100)
  ],
  GainNode: [
    new NumberParameter("gain", 0, 3, "exponantial")
  ],
  DelayNode: [
    new NumberParameter("delayTime", 0, 1, "exponantial")
  ],
  BiquadFilterNode: [
    new NumberParameter("frequency", 0, 2000),
    new NumberParameter("Q", -100, 100)
  ]
}

export function getParameters(nodeName: string, paramName: string) {
  const index = nodeName as ParametersEnum;
  return parameters[index].find(({ name }) => paramName === name);
}

export { parameters }
