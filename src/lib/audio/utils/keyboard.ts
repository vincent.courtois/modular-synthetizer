export interface INote {
  // The name of the note in the French notation (do-re-mi)
  name: string;
  // The base frequency of the note, based on la-440
  pitch: number;
  // Determines if this note is followed by a black note
  hasBlack: boolean;
  // Gives the frequency of the black note if it exists
  black?: number;
  // the key on the physical keyboard bound to this virtual note
  key: string,
  // The black key on the physical keyboard
  blackKey?: string
}

// This represents the notes in an octave, and helps us draw the keyboard
export const notes: INote[] = [
  { name: "do4", pitch: 261.63, hasBlack: true, black: 277.18, key: "Q", blackKey: "2" },
  { name: "ré4", pitch: 293.66, hasBlack: true, black: 311.13, key: "W", blackKey: "3" },
  { name: "mi4", pitch: 329.63, hasBlack: false, key: "E" },
  { name: "fa4", pitch: 349.23, hasBlack: true, black: 369.99, key: "R", blackKey: "5" },
  { name: "sol4", pitch: 392, hasBlack: true, black: 415.3, key: "T", blackKey: "6" },
  { name: "la4", pitch: 440, hasBlack: true, black: 466.16, key: "Y", blackKey: "7" },
  { name: "si4", pitch: 493.88, hasBlack: false, key: 'U' },
  { name: "do5", pitch: 523.25, hasBlack: true, black: 554.37, key: "Z", blackKey: "S" },
  { name: "ré5", pitch: 587.33, hasBlack: true, black: 622.25, key: "X", blackKey: "D" },
  { name: "mi5", pitch: 659.26, hasBlack: false, key: "C" },
  { name: "fa5", pitch: 698.46, hasBlack: true, black: 739.99, key: "V", blackKey: "G" },
  { name: "sol5", pitch: 783.99, hasBlack: true, black: 830.61, key: "B", blackKey: "H" },
  { name: "la5", pitch: 880, hasBlack: true, black: 932.33, key: "N", blackKey: "J" },
  { name: "si5", pitch: 987.77, hasBlack: false, key: "M" },
]