import { createOscillator } from '@/lib/audio/factories/createOscillator'
import { createGain } from '../factories/createGain'
import { createDestination } from '../factories/createDestination'
import { createMicrophone } from '../factories/createMicrophone'
import { createBiquadFilter } from '../factories/createBiquadFilter'
import { createDelay } from '../factories/createDelay'
import { createChannelSplitter } from '../factories/createChannelSplitter'
import { createChannelMerger } from '../factories/createChannelMerger'
import { createKeyboard } from '../factories/createKeyboard'
import { KeyboardWrapper } from '@/lib/gui/wrappers/KeyboardWrapper'
import { createAnalyserNode } from '../factories/createAnalyserNode'
import { AnalyserWrapper } from '@/lib/gui/wrappers/AnalyserWrapper'
import { AnalyserWaveWrapper } from '@/lib/gui/wrappers/AnalyserWaveWrapper'

const tools = [
  {
    "name": "Oscillators",
    "icon": "mdi-sine-wave",
    "items": [
      {
        icon: 'mdi-sine-wave',
        label: 'Oscillator',
        factory: createOscillator("sine")
      },
      {
        icon: 'mdi-square-wave',
        label: 'Square Oscillator',
        factory: createOscillator("square")
      },
      {
        icon: 'mdi-triangle-wave',
        label: 'Triangle Oscillator',
        factory: createOscillator("triangle")
      },
      {
        icon: 'mdi-sawtooth-wave',
        label: 'Sawtooth Oscillator',
        factory: createOscillator("sawtooth")
      }
    ]
  },
  {
    "name": "Other inputs",
    "icon": "mdi-microphone",
    "items": [
      {
        icon: 'mdi-microphone',
        label: 'Microphone input',
        factory: createMicrophone
      },
      {
        icon: "mdi-piano",
        label: "Piano keyboard",
        factory: createKeyboard,
        wrapper: KeyboardWrapper
      }
    ]
  },
  {
    "name": "Biquad filters",
    "icon": "mdi-arrow-expand-vertical",
    "items": [
      {
        "icon": "mdi-arrow-expand-up",
        "label": "Lowpass node",
        factory: createBiquadFilter("lowpass")
      },
      {
        "icon": "mdi-arrow-expand-down",
        "label": "Highpass node",
        factory: createBiquadFilter("highpass")
      }
    ]
  },
  {
    "name": "Filters",
    "icon": "mdi-air-filter",
    "items": [
      {
        icon: 'mdi-volume-high',
        label: 'Gain node',
        factory: createGain
      },
      {
        icon: 'mdi-progress-clock',
        label: 'Delay node',
        factory: createDelay
      },
      {
        icon: "mdi-call-split",
        label: "Channel splitter",
        factory: createChannelSplitter
      },
      {
        icon: "mdi-call-merge",
        label: "Channel merger",
        factory: createChannelMerger
      }
    ]
  },
  {
    name: "Other",
    icon: 'mdi-cog-outline',
    items: [
      {
        icon: 'mdi-chart-bell-curve-cumulative',
        label: 'Oscilloscope node',
        factory: createAnalyserNode,
        wrapper: AnalyserWaveWrapper,
      },
      {
        icon: 'mdi-chart-bar',
        label: 'Bar frequency node',
        factory: createAnalyserNode,
        wrapper: AnalyserWrapper,
      }
    ]
  },
  {
    "name": "Outputs",
    "icon": "mdi-speaker",
    "items": [
      {
        icon: 'mdi-speaker',
        label: 'Default output',
        factory: createDestination
      }
    ]
  }
]

export { tools }
