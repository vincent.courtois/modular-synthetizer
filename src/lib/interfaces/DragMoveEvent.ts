export interface DragMoveEvent {
  target: {
    attrs: {
      x: number,
      y: number,
    }
  }
}