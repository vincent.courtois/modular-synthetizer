import store from "@/store";

interface Coordinates {
  x: number,
  y: number;
}

/**
 * Translates normal coordinates in coordinates in the stage. The stage has a
 * different system lof coordinates because we can :
 * 1. zoom in and out of the stage, so that the scale of each dimension is either
 *    reduced or enhanced.
 * 2. move the whole stage, so that the origin point is moved and differs from the
 *    HTML one.
 */
export function translate({x, y}: Coordinates): Coordinates {
  return {
    x: (x - store.state.dragDiff.x) / store.state.scale,
    y: (y - store.state.dragDiff.y) / store.state.scale
  };
}