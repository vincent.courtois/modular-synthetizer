export const WDT = 200;

// This value is kind of magic. It has been determined after trials and
// errors as the best value to apply to the wheel scroll to zoom in and
// out smoothly without it being too fast or too slow.
export const ZOOM_RATIO = 0.005;

// This value is the maximum value the ratio can be "zoomed out" with
// It is a magic value obtained after some trials and errors too.
export const MAX_ZOOM_OUT = 0.0625

// This value is the maximum value the ratio can be "zoomed in" with
// It is a magic value obtained after some trials and errors too.
export const MAX_ZOOM_IN = 2;

export const MENU_WIDTH = 240;

export const MENU_MARGIN = 20;

export const PORTS_RADIUS = 12;

export const BETWEEN_PORTS = 10

// The interval, in pixels, between the centers of two ports of the same group.
export const PORTS_INTERVAL = 2 * PORTS_RADIUS + BETWEEN_PORTS;


/** Constants about keyboard keys dimensions */
export const KEYBOARD_TOP_PADDING = 30;
export const WHITE_KEY_WIDTH = 50;
export const WHITE_KEY_HEIGHT = 150;
export const BLACK_KEY_WIDTH = 30;
export const BLACK_KEY_HEIGHT = 100;

/** Visualiser constants **/
export const VISUALIZER_PRECISION = 1024;
export const FREQUENCY_BAR_WIDTH = 1;
export const FREQUENCY_BAR_HEIGHT = 200;
export const VISUALIZER_MARGIN = 20;
export const VISUALIZER_MARGIN_TOP = 100;

export const SLIDER_WIDTH = 120;