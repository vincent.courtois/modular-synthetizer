import { WDT, MENU_WIDTH, MENU_MARGIN, PORTS_INTERVAL } from "../utils/constants";
import { NodeWrapper } from "../wrappers/NodeWrapper";
import Konva from "konva";
import { CloseButton } from "./elements/CloseButton";
import { NodeTitle } from "./elements/NodeTitle";
import { ParamWrapper } from "../wrappers/ParamWrapper";
import { translate } from "../utils/translate";

const HGT: number = 150;

/**
 * Representation of a node in the GUI as a Konva configuration object.
 * This holds the configurations for all the objects related directly
 * to the node :
 * - Rectangle container of the node
 * - Text for the title of the node
 * - Text for the close button of the node
 * - Rectangle container for the parameters list.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export class NodeConfiguration extends Konva.Rect {
  wrapper: NodeWrapper;

  // Configuration for the rectangle wrapping the whole node.
  container: Konva.Rect = new Konva.Rect({
    width: WDT,
    height: HGT,
    fill: '#F5F5F5',
    stroke: '#C2C2C2',
    shadowColor: "black",
    shadowOffset: {x: 5, y: 5},
    shadowOpacity: 0.15,
    shadowBlur: 10,
    cornerRadius: 20
  })

  // Configuration for the text title of the node.
  title: NodeTitle = new NodeTitle();

  // Configuration for the close button at the top right of the node.
  close: CloseButton = new CloseButton()

  // Configuration for the wrapper for all parameters.
  paramsContainer: Konva.Rect = new Konva.Rect({x: 15, y: 25, width: 150});

  /**
   * Creates a configuration from a declared NodeWrapper wrapping an
   * AudioNode. This is necessary as for the title we need the name
   * of the node, or for the inputs/outputs we need the lists of ports.
   * 
   * @param wrapper The wrapper of an AudioNode from the web audio API
   *   to create a configuration for.
   */
  constructor(wrapper: NodeWrapper) {
    super({
      ...translate({
        x: MENU_WIDTH + 2 * MENU_MARGIN,
        y: MENU_MARGIN
      }),
      height: HGT,
      width: WDT
    })
    this.wrapper = wrapper;
  }

  absoluteX() {
    return this.absolutePosition().x
  }

  absoluteY() {
    return this.absolutePosition().y
  }

  /**
   * Gets the position from the top of the node where the container for the
   * given ports should be placed to have an harmonious display.
   * 
   * @param portsList Gets the Y origin of a serie of ports (inputs or outputs) given
   *   the number of ports to place. The goal is that the serie of ports will be
   *   vertically centered around the vertical center of the node.
   * @returns the number of pixels from the top of the node where the container for
   *   this serie of ports should begin.
   */
  portsOrigin(portsList: any[]) {
    return {x: 0, y: (this.height() - PORTS_INTERVAL * (portsList.length - 1)) / 2}
  }

  inputsOrigin() {
    return this.portsOrigin(this.wrapper.inputs)
  }

  outputsOrigin() {
    return this.portsOrigin(this.wrapper.outputs)
  }

  /**
   * Updates the coordinates of the node element and propagates the modification to :
   * - the links output and input of the current node
   * - the links input into the parameters of the current node
   * 
   * @param x the number of pixels from the left of the canva where the item is now placed
   * @param y the number of pixels from the top of the canva where the item is now placed
   */
  updateCoordinates(x: number, y: number) {
    this.position({x, y});
    this.refreshParameters()
    this.refreshLinks();
  }

  refreshLinks() {
    this.wrapper.ports.forEach(p => p.config.refreshLinks());
  }

  refreshParameters() {
    this.wrapper.params.forEach((param: ParamWrapper) => {
      param.updateInputsCoordinates(this.x(), this.y())
    });
  }
  wholeWidth() {
    return this.width();
  }
}