import { PORTS_INTERVAL, PORTS_RADIUS, WDT } from "../utils/constants";
import { PortWrapper } from "../wrappers/PortWrapper";
import Konva from "konva"
import { LinkWrapper } from "../wrappers/LinkWrapper";


export class PortConfiguration extends Konva.Circle {
  wrapper: PortWrapper;
  input: boolean = false;
  displayMagnet: boolean = false;

  /**
   * 
   * @param wrapper 
   * @param position 
   * @param npPorts the current total number of ports
   */
  constructor(wrapper: PortWrapper, position: number, input: boolean) {
    super({
      y: position * PORTS_INTERVAL,
      x: wrapper.node.config.wholeWidth(),
      radius: PORTS_RADIUS,
      fill: '#81d4fa',
      stroke: '#4ba3c7'
    });
    this.wrapper = wrapper;
    this.input = input;
    if (this.input) {
      this.makeStartingPort()
    }
  }

  public get absPosition() {
    const cfg = this.wrapper.node.config;
    const origin: any = this.input? cfg.inputsOrigin() : cfg.outputsOrigin()
    return {
      x: this.x() + cfg.absoluteX(),
      y: origin.y + this.y() + cfg.absoluteY()
    }
  }

  public points() {
    return [
      this.absPosition.x,
      this.absPosition.y
    ]
  }

  public get nodeCfg() {
    return this.wrapper.node.config;
  }

  refreshLinks() {
    this.wrapper.connectedTo.forEach((link: LinkWrapper) => {
      link.config.trace()
    })
    this.wrapper.connectedFrom.forEach((link: LinkWrapper) => {
      link.config.trace()
    })
  }

  makeStartingPort() {
    this.x(0)
    this.fill('#c5e1a5');
    this.stroke('#94af76');
  }

}