import Konva from "konva";
import { FREQUENCY_BAR_HEIGHT, FREQUENCY_BAR_WIDTH, VISUALIZER_MARGIN, VISUALIZER_MARGIN_TOP, VISUALIZER_PRECISION } from "../utils/constants";
import { NodeWrapper } from "../wrappers/NodeWrapper";
import { CloseButton } from "./elements/CloseButton";
import { NodeConfiguration } from "./NodeConfiguration";

const analyserHeight = VISUALIZER_MARGIN_TOP + FREQUENCY_BAR_HEIGHT + 2 * VISUALIZER_MARGIN;
const analyserWidth = VISUALIZER_PRECISION / 2 * FREQUENCY_BAR_WIDTH + 2 * VISUALIZER_MARGIN;

export class AnalyserConfiguration extends NodeConfiguration {

  // Configuration for the rectangle wrapping the whole node.
  container: Konva.Rect = new Konva.Rect({
    width: analyserWidth,
    height: analyserHeight,
    fill: '#F5F5F5',
    stroke: '#C2C2C2',
    shadowColor: "black",
    shadowOffset: {x: 5, y: 5},
    shadowOpacity: 0.15,
    shadowBlur: 10,
    cornerRadius: 20
  })

  close: CloseButton = new CloseButton(analyserWidth)

  constructor(wrapper: NodeWrapper) {
    super(wrapper);
    this.container.width(analyserWidth)
    this.width(analyserWidth)
    this.height(analyserHeight)
    this.container.height(analyserHeight)
  }

}
