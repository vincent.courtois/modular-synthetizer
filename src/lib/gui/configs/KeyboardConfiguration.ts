import { BLACK_KEY_HEIGHT, BLACK_KEY_WIDTH, KEYBOARD_TOP_PADDING, WHITE_KEY_HEIGHT, WHITE_KEY_WIDTH } from "../utils/constants";
import { KeyboardWrapper } from "../wrappers/KeyboardWrapper";
import { NodeWrapper } from "../wrappers/NodeWrapper";
import { NodeConfiguration } from "./NodeConfiguration";

export class KeyboardConfiguration extends NodeConfiguration {

  wrapper: KeyboardWrapper;

  constructor(wrapper: NodeWrapper) {
    super(wrapper);
    this.wrapper = wrapper as KeyboardWrapper;
    this.container.width(700)
    this.width(700)
  }

  /**
   * Creates the configuration for a white key on the virtual keyboard.
   * The configuration represents only the white rectangle with a black
   * stroke around it.
   * @param index the index of the key in the array of keys, determining
   *   its position in the virtual keyboard displayed.
   * @param name the name of the note, used to know if the key is currently
   *   being played or not (if it is played, the key changes color)
   * @returns The created configuration as a hash
   */
  createWhiteKey(index: number, name?: string) {
    return {
      x: WHITE_KEY_WIDTH * index,
      y: KEYBOARD_TOP_PADDING,
      width: WHITE_KEY_WIDTH,
      height: WHITE_KEY_HEIGHT,
      fill: this.getFill("white", "silver", name),
      stroke: "black"
    }
  }

  // Same than above method for black keys.
  createBlackKey(index: number, name?: string) {
    return {
      x: WHITE_KEY_WIDTH * (index + 1) - (BLACK_KEY_WIDTH / 2),
      y: KEYBOARD_TOP_PADDING,
      width: BLACK_KEY_WIDTH,
      height: BLACK_KEY_HEIGHT,
      stroke: "black",
      fill: this.getFill("black", "silver", name),
    }
  }

  /**
   * Gets the fill color for the a key, depending on whether she's
   * currently being played or not.
   * @param notPressed the color if the key is NOT played
   * @param pressed the color if the key is being played
   * @param name the name of the note, used to know if it is being played
   * @returns a string representing the color for the background of the key
   */
  getFill(notPressed: string, pressed: string, name?: string) {
    if (name === undefined) return notPressed;
    const played: boolean = this.wrapper.keys[name].played == true;
    return played ? pressed: notPressed
  }

  /**
   * Creates the configuration to display an indication on the virtual keyboard
   * saying which key of the real keyboard should be played to play the
   * corresponding note.
   * 
   * @param config the configuration of the key to associate the tooltip to
   * @param text the text content of the tooltip, usually the key you have
   *   to hit to play the note.
   * @returns the configuration of the tooltip as a hash
   */
  createTooltip(config: any, text: string) {
    return {
      x: config.x,
      width: config.width,
      y: config.y + config.height - 20,
      text,
      align: 'center'
    }
  }

  createWhiteTooltip(index: number, text: string) {
    const config = this.createWhiteKey(index)
    return this.createTooltip(config, text)
  }

  createBlackTooltip(index: number, text: string) {
    const config = this.createBlackKey(index)
    return {
      ...this.createTooltip(config, text),
      fill: "white"
    }
  }
}