import { WDT } from "@/lib/gui/utils/constants"
import Konva from "konva";

export class NodeTitle extends Konva.Text {
  constructor() {
    super({
      fontFamily: 'Roboto, sans-serif',
      fontSize: 16,
      lineHeight: 2,
      x: 25,
      y: 5,
      width: WDT - 30,
      verticalAlign: 'center'
    });
  }
}