import { WDT } from "@/lib/gui/utils/constants"
import Konva from "konva";

export class CloseButton extends Konva.Text {
  constructor(width: number = WDT) {
    super({
      fontFamily: 'FontAwesome',
      fontStyle: 'normal 200',
      x: WDT - 55,
      y: 10,
      width: 30,
      height: 20,
      align: 'right',
      verticalAlign: 'center',
      fontSize: 18,
      lineHeight: 1,
    });
  }
}