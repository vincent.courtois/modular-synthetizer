import { LinkWrapper } from "../wrappers/LinkWrapper";
import Konva from "konva"
import { PortConfiguration } from "./PortConfiguration";

/**
 * This class represents the configuration of a link between two nodes.
 * It does not make suppositions about the nature of the receiving end
 * of the connection, whether it be another node or a parameter.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export class LinkConfiguration {
  strokeWidth: number = 3;
  stroke: string = 'black';
  wrapper: LinkWrapper;
  points: number[] = [];
  isHovered: boolean = false;
  startCircle: Konva.Circle = new Konva.Circle({fill: "black", radius: 3})
  endCircle: Konva.Circle = new Konva.Circle({fill: "black", radius: 3})

  constructor(wrapper: LinkWrapper) {
    this.wrapper = wrapper;
    this.trace();
  }

  onHoverChange(): void {
    this.isHovered = !this.isHovered
    this.stroke = this.isHovered ? 'red' : 'black';
  }

  /**
   * Traces all the elements of a link :
   * - the line between the two ports
   * - the little dots at each end of the line
   */
  trace() {
    this.points = [
      ...this.wrapper.start.config.points(),
      ...this.wrapper.end.config.points(),
    ]
    this.refreshDots()
  }

  /**
   * Creates the little dot at the end of the link between two ports.
   * @param port the port configuration to get the position from.
   * @returns the created konva Circle instance initialized.
   */
  circleFrom(port: PortConfiguration) {
    return new Konva.Circle({
      fill: "black",
      radius: 3,
      x: port.absPosition.x,
      y: port.absPosition.y
    })
  }

  refreshDots() {
    this.startCircle = this.circleFrom(this.wrapper.start.config)
    this.endCircle = this.circleFrom(this.wrapper.end.config)
  }
}