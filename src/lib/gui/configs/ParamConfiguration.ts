import Konva from "konva";
import { Vector2d } from "konva/lib/types";
import { ParamWrapper } from "../wrappers/ParamWrapper";
import { getParameters } from '@/lib/audio/utils/parameters'
import { state } from "@/store/modules/AudioNodes/state"

const MARGIN: number = 10;
const HEIGHT: number = 40;

interface IBounds {
    max: number;
    min: number;
    delta: number;
}

/**
 * Configuration for a parameter in the UI. This holds the
 * configuration for the text, the box, and helps place the
 * input ports of the parameter.
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export class ParamConfiguration {

  position: number = 0;
  x: number = 0;
  y: number = 10;
  width: number = 150;
  height: number = 40;
  stroke: string = '#C2C2C2';

  wrapper: ParamWrapper;

  lineConfig: Konva.Line;

  circleConfig: Konva.Circle;

  value: number = 0;

  circleCoords = {x: 0, y: 0}

  // This ratio is used to get the exponantial values correctly when setting a value.
  expRatio: number = 1;

  constructor(wrapper: ParamWrapper) {
    this.wrapper = wrapper;
    this.value = wrapper.param.value;
    this.lineConfig = new Konva.Line({
      x: 30,
      y: 25 + 50 * this.position,
      points: [120, 0, 0, 0, 0, 0, 0, 0],
      stroke: "#AAAAAA",
      strokeWidth: 6,
      visible: true,
      name: "trackLine",
      lineCap: "square"
    });
    const vm = this;
    const bounds = this.getBounds();
    const initX = this.wrapper.param.value / bounds.delta * 120
    this.expRatio = Math.log(this.getBounds().delta) / this.lineConfig.points()[0]
    this.circleConfig = new Konva.Circle({
      x: 30 + initX,
      y: 25 + 50 * this.position,
      stroke: "#AAAAAA",
      fill: "white",
      strokeWidth: 2,
      radius: 8,
      draggable: true,
      dragOnTop: true,
      visible: true,
      dragBoundFunc: function(pos: any) {
        const xDiff = vm.absoluteX()
        const x = Math.min(150, Math.max(30, pos.x - xDiff));
        const coords = {
          x: x + xDiff,
          y: this.getAbsolutePosition().y
        };
        vm.circleConfig.x(x);
        return coords;
      }
    });
  }

  placeAt(position: number) {
    this.position = position;
    this.resetCoordinates();
  }

  resetCoordinates() {
    this.y = MARGIN + (HEIGHT + MARGIN) * this.position;
  }

  absoluteCoordinates(): Vector2d {
    const nodeConfig: any = this.wrapper.node.config
    const x = nodeConfig.x() + nodeConfig.paramsContainer.x() + this.x + 10;
    const y = nodeConfig.y() + nodeConfig.paramsContainer.y() + this.y;
    return {x, y}
  }

  absoluteX(): number {
    return this.absoluteCoordinates().x
  }

  absoluteY(): number {
    return this.absoluteCoordinates().y
  }

  portsOrigin(ports: any[] = []) { return {x: 0, y: HEIGHT / 2}; }

  inputsOrigin() { return this.portsOrigin(); }

  outputsOrigin() { return this.portsOrigin(); }

  getBounds(): IBounds {
    const {minValue, maxValue} = this.wrapper.getConfiguration();
    return {
      min: minValue,
      max: maxValue,
      delta: maxValue - minValue
    };
  }

  updateValue(value?: number) {
    if (value === undefined || state.context === undefined) return;
    this.wrapper.param.setValueAtTime(value, state.context.currentTime);
  }

  wholeWidth() {
    return this.width;
  }

  updatePositionFrom(value: number) {
    this.circleConfig.x(this.wrapper.strategy.setX(value));
  }
}