import { LinkConfiguration } from "../configs/LinkConfiguration";
import { PortWrapper } from "../wrappers/PortWrapper";
import { ParamWrapper } from "./ParamWrapper";
import { v4 as uuid } from 'uuid';

interface ILinkWrapper {
  // The starting port of the link
  start: PortWrapper;
  // The ending port of the link
  end: PortWrapper;
  // The Konva configuration associated to this wrapper.
  config: LinkConfiguration;
}

/**
 * A link wrapper represents the connection between two ports
 * of two different nodes.
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export class LinkWrapper implements ILinkWrapper {
  id: string;
  start: PortWrapper;
  end: PortWrapper;
  config: LinkConfiguration;

  constructor(start: PortWrapper, end: PortWrapper) {
    this.start = start;
    this.end = end;
    this.id = uuid();
    this.config = new LinkConfiguration(this);
  }
}
