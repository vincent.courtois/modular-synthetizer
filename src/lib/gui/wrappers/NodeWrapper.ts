import { IParameter } from "@/lib/audio/utils/parameters/IParameter"
import { parameters, ParametersConfiguration } from "@/lib/audio/utils/parameters"
import { NodeConfiguration } from "../configs/NodeConfiguration";
import { ParamWrapper } from "./ParamWrapper";
import { makeOutputable } from "./mixins/makeOutputable";
import { makeInputable } from "./mixins/makeInputable";
import { createPorts } from "./factories/createPorts";
import { v4 as uuid } from "uuid";
import { PortWrapper } from "./PortWrapper";
import { LinkWrapper } from "./LinkWrapper";

// Uses the mixins to create a new class both inputable AND outputable.
const PluggableNode = makeInputable(makeOutputable(class {}))

/**
 * A node wrapper just wraps an AudioNode instance to be able to access
 * vanity methods such as, but not limited to :
 * - The list of inputs and outputs of the node
 * - The list of parameters of the node wrapped as ParamWrapper instances
 *   (and thus their own inputs and outputs)
 * 
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export class NodeWrapper<T extends AudioNode = AudioNode> extends PluggableNode {
  id: string;
  // This is th wrapped Web audio API audio node.
  node: T;
  // This is an additionnal name to be editable by user.
  name: string;
  // This is the associated Konva configuration
  config: NodeConfiguration;

  params: ParamWrapper[] = [];

  component: string;

  constructor(node: T, name: string) {
    super();
    this.id = uuid();
    this.node = node;
    this.name = name;
    this.component = "DefaultNode";
    this.config = new NodeConfiguration(this);
    this.setParams();

    this.inputs = createPorts(this, this.node.numberOfInputs, true)
    this.outputs = createPorts(this, this.node.numberOfOutputs)
  }

  setParams() {
    const cName: string = this.node.constructor.name;
    const pNames: IParameter[] = parameters[cName as keyof ParametersConfiguration];
    if (pNames == undefined) return;
    pNames.forEach(name => {
      try {
        const value: AudioParam = (this.node[name.name as keyof AudioNode] as unknown as AudioParam);
        this.params.push(new ParamWrapper(this, this.params.length, value as AudioParam, name.name));
      }
      catch {}
    });
  }

  /**
   * Disconnects this wrapper from all its linked wrappers, essentially
   * disconnecting the underlying node from all its siblings.
   */
  disconnect() {
    this.node.disconnect();
  }

  /**
   * Disconnects the current node wrapper from the audio tree and removes
   * all links directed to it, and emerging from it so that it is not
   * included after when doing a refresh.
   */
  removeLinks() {
    this.removeInputs();
    this.removeOutputs();
  }

  public get ports(): PortWrapper[] {
    return [...this.inputs, ...this.outputs]
  }

  public get links(): LinkWrapper[] {
    const results: LinkWrapper[] = []
    this.inputs.forEach((port: PortWrapper) => {
      port.connectedFrom.forEach((link: LinkWrapper) => {
        results.push(link);
      })
    })
    this.outputs.forEach((port: PortWrapper) => {
      port.connectedTo.forEach((link: LinkWrapper) => {
        results.push(link);
      })
    })
    return results
  }
}

(window as any).NodeWrapper = NodeWrapper;
