import store from "@/store";
import { PortWrapper } from "../PortWrapper";
import { ConnectableConstructor } from "../types/ConnectableConstructor";

export function makeOutputable<T extends ConnectableConstructor<{}>>(Superclass: T) {
  /**
   * A class extending this class can be connecting other nodes as its INPUT,
   * so that the signal from the connected node can be forwarded to this node.
   * @author Vincent Courtois <vincent.courtois@coddity.com>
   */
  return class ConnectableFrom extends Superclass {
    
    outputs: PortWrapper[] = [];

    constructor(...args: any[]) {
      super();
    }

    removeOutputs() {
      this.outputs.forEach((output: PortWrapper) => {
        store.commit("deleteLinks", output.connectedTo);
      })
    }

    updateOutputsCoordinates(x: number, y: number) {
      this.outputs.forEach((output: PortWrapper) => {
        output.propagateCoordinates(x, y)
      })
    }
  }
}