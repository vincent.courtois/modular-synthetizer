import store from "@/store";
import { PortWrapper } from "../PortWrapper";
import { ConnectableConstructor } from "../types/ConnectableConstructor";

export function makeInputable<T extends ConnectableConstructor<{}>>(Superclass: T) {
  /**
   * A class extending this class can be connecting other nodes as its INPUT,
   * so that the signal from the connected node can be forwarded to this node.
   * @author Vincent Courtois <vincent.courtois@coddity.com>
   */
  return class ConnectableFrom extends Superclass {
    
    inputs: PortWrapper[] = [];

    constructor(...args: any[]) {
      super();
    }

    removeInputs() {
      this.inputs.forEach((input: PortWrapper) => {
        store.commit("deleteLinks", input.connectedFrom);
      })
    }

    updateInputsCoordinates(x: number, y: number) {
      this.inputs.forEach((input: PortWrapper) => {
        input.propagateCoordinates(x, y)
      })
    }
  }
}