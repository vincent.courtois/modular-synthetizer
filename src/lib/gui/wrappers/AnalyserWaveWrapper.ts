import { AnalyserConfiguration } from "../configs/AnalyserConfiguration";
import { createPorts } from "./factories/createPorts";
import { NodeWrapper } from "./NodeWrapper";

export class AnalyserWaveWrapper extends NodeWrapper<AnalyserNode> {

  constructor(node: AnalyserNode, label: string) {
    super(node, label);
    this.component = 'AnalyserWaveNode';
    this.config = new AnalyserConfiguration(this);
    this.outputs = createPorts(this, this.node.numberOfOutputs);
  }
}

(window as any).AnalyserWaveWrapper = AnalyserWaveWrapper;
