import { NodeWrapper } from "../NodeWrapper";
import { ParamWrapper } from "../ParamWrapper";
import { PortWrapper } from "../PortWrapper";

/**
 * Creates a set of ports for a given container. A container can either be an instance
 * of ParamWrapper or an instance of NodeWrapper.
 * 
 * @param container the object (parameter or node) containing the created ports
 * @param portsCount the number of ports you want to create for this container
 * @param areInputs TRUE if the ports are input ports, FALSE otherwise
 * @returns a set of PortWrapper instances link"ed to the container
 */
export function createPorts(container: ParamWrapper|NodeWrapper, portsCount: number, inputs: boolean = false) {
  const ports: PortWrapper[] = [];
  for(let i = 0; i < portsCount; ++i) {
    ports.push(new PortWrapper(container, i, inputs));
  }
  return ports;
}
