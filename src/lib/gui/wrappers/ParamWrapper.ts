import { ParamConfiguration } from "../configs/ParamConfiguration";
import { createPorts } from "./factories/createPorts";
import { makeInputable } from "./mixins/makeInputable";
import { NodeWrapper } from "./NodeWrapper";
import { PortWrapper } from "./PortWrapper";
import { IStrategy } from "@/lib/audio/utils/parameters/strategies/IStrategy";
import getStrategy from "@/lib/audio/utils/parameters/strategies";
import { getParameters } from "@/lib/audio/utils/parameters";

const PluggableParam = makeInputable(class {});

export class ParamWrapper extends PluggableParam {
  config: ParamConfiguration;
  param: AudioParam;
  name: string;
  node: NodeWrapper|ParamWrapper;
  outputs: PortWrapper[] = [];
  position: number;
  value: number = 0;
  strategy: IStrategy;

  constructor(node: NodeWrapper, position: number, param: AudioParam, name: string) {
    super()
    this.param = param;
    this.name = name;
    this.node = node;
    this.config = new ParamConfiguration(this);
    this.config.placeAt(position);
    this.inputs = createPorts(this, 1, true)
    this.position = position;
    this.value = this.param.value;

    this.strategy = getStrategy(this.getConfiguration().strategy, this);
    this.strategy.setX(this.param.value);
  }

  updateValue() {
    this.strategy.setValue(this.config.circleConfig.x() - 30);
  }

  getConfiguration() {
    const className = this.node.node.constructor.name;
    return getParameters(className, this.name) as any;
  }
}
