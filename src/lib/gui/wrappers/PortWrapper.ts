import { PortConfiguration } from "../configs/PortConfiguration";
import { LinkWrapper } from "./LinkWrapper";
import { NodeWrapper } from "./NodeWrapper";
import store from '@/store'
import { ParamWrapper } from "./ParamWrapper";

/**
 * Wrapper around an audio node of the web audio API.
 * @author Vincent Courtois <vincent.courtois@coddity.com>
 */
export class PortWrapper {
  // The other ports this one connects to.
  connectedTo: LinkWrapper[] = [];
  // The other ports connected to this one.
  connectedFrom: LinkWrapper[] = [];
  // The node to which this port is attached.
  node: NodeWrapper|ParamWrapper;
  // The Konva configuration for this port.
  config: PortConfiguration;

  input: boolean = false;

  position: number;

  constructor(node: NodeWrapper|ParamWrapper, position: number, input: boolean) {
    this.node = node;
    this.position = position;
    this.input = input;
    this.config = new PortConfiguration(this, position, input);
  }

  connect(port: PortWrapper) {
    const link = new LinkWrapper(this, port);
    this.connectedTo.push(link);
    port.connectedFrom.push(link);
    store.commit("addLink", link)
  }

  propagateCoordinates(x: number, y: number) {
    this.connectedTo.forEach((link: LinkWrapper) => {
      link.config.trace();
    });
    this.connectedFrom.forEach((link:LinkWrapper) => {
      link.config.trace();
    })
  }
}