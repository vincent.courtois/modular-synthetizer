import { KeyboardConfiguration } from "../configs/KeyboardConfiguration";
import { createPorts } from "./factories/createPorts";
import { NodeWrapper } from "./NodeWrapper";

interface Key {
  oscillator: OscillatorNode;
  gain: GainNode;
  played: boolean;
}

type Keys = {[key: string]: Key}

interface KeyboardPayload {
  node: GainNode;
  keys: Keys;
}

export class KeyboardWrapper extends NodeWrapper {
  keys: Keys;
  
  constructor({node, keys}: KeyboardPayload, label: string) {
    super(node, label);
    this.component = "KeyboardNode"
    this.keys = keys;
    this.config = new KeyboardConfiguration(this);
    this.outputs = createPorts(this, this.node.numberOfOutputs)
  }

  attackNote(name: string) {
    if (!(name in this.keys)) return;

    const gain = this.keys[name].gain
    const now = gain.context.currentTime;
    this.keys[name].played = true;
    gain.gain.cancelScheduledValues(now);
    gain.gain.exponentialRampToValueAtTime(1, now + 0.05);
    gain.gain.linearRampToValueAtTime(0.6, now + 0.15);
  }

  releaseNote(name: string) {
    const gain = this.keys[name].gain
    const now = gain.context.currentTime;
    gain.gain.cancelScheduledValues(now);
    gain.gain.linearRampToValueAtTime(0, now + 0.25)
    this.keys[name].played = false;
  }
}

(window as any).KeyboardWrapper = KeyboardWrapper